import React, { useState } from 'react';
import { Modal } from 'react-bootstrap';

const TodoList = (prop) => {

  // edit todo input ref
  const newTodoRef = React.createRef();

  // delete todo handler
  const onDeleteHandler = (todoitem) => {
    prop.deleteTodo(todoitem);
  }

  // toggle todo handler
  const onChangeHandler = (todoitem) => {
    prop.toggleTodo(todoitem);
  }

  // edit todo handler
  const onEditHandler = (todoitem) => {
    const newTodoTitle = newTodoRef.current.value;
    
    if(newTodoTitle.length !== 0) {
      prop.editTodo(todoitem, newTodoTitle)
    }
  }

  // edit todo modal show
  const EditTodoButton = (todo) => {
			const [show, setShow] = useState(false);

			const handleClose = () => setShow(false);
			const handleShow = () => setShow(true);

			return (
				<>
          <button className="btn btn-info text-white mr-2" onClick={handleShow}>Edit</button>

					<Modal show={show} onHide={handleClose}>
						<Modal.Header closeButton>
							<Modal.Title>Edit Todo</Modal.Title>
						</Modal.Header>
              <Modal.Body>
                <form onSubmit={() => onEditHandler(todo)}>
                  <input type="text" className="form-control" placeholder="Enter the new todo" ref={newTodoRef}/>
                </form>
              </Modal.Body>
              <Modal.Footer>
                <button type="button" className="btn btn-secondary" onClick={handleClose}>Close</button>
                <button type="submit" className="btn btn-primary" onClick={() => onEditHandler(todo)}>Save</button>
              </Modal.Footer>
					</Modal>
				</>
			);
		}

  // render todo when fetch todo is not null
  const RenderTodos = () => {
    if(prop.fetchTodos !== null) {
      return (
        prop.fetchTodos.map((todo) => {
          return (
            <>
              <div className="d-flex justify-content-between align-items-center px-3"> 
                  <div className="d-flex align-items-center">
                      <input type="checkbox" className="mt-1" onChange={() => onChangeHandler(todo)} defaultChecked={todo.isDone}></input>
                      <p className="ml-2 mt-3">{todo.title}</p>
                  </div>
                  <div>
                      <EditTodoButton todo={todo}/>
                      <button className="btn btn-danger delete" onClick={() => onDeleteHandler(todo)}>Delete</button>
                  </div>
              </div>
              <hr className="m-0"></hr>
            </>
          )
        })
      )
    } else {
      return <div></div>
    }
  };
  
  return (
    <div className="container mt-1">
        <div className="d-flex justify-content-between align-items-center">
            <h3>Your todos:</h3>
        </div>
        <div className="rounded shadow-sm mt-3">
            <RenderTodos />
        </div>
    </div> 
  )
}

export default TodoList;

