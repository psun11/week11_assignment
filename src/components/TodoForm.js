import React, { useState } from 'react';

const TodoForm = (prop) => {

    const [input, setInput] = useState("");

	// input changes handler
    const handleChange = (e) => {
        setInput(e.target.value);
    }

	// add new todo submit handler
    const onSubmitHandler = (e) => {
        e.preventDefault();

        if (input.length !== 0) {
            const newTodoItem = {
                title: input,
                isDone: false
            }

            prop.addTodo(newTodoItem)
        }
        setInput("");
	}
		
    return (
        <div className="container">
            <h1 className="my-5">TASK 1: ToDo List with React</h1>
            <form onSubmit={onSubmitHandler}>
                <div className="d-flex mb-4">
                    <input type="text" className="w-25 mr-3 p-2 rounded border border-secondary" placeholder="Enter todo" value={input} onChange={handleChange}/>
                    <button className="btn btn-secondary">Add todo</button>
                </div>
            </form>
            <hr/>
        </div>
    );
}

export default TodoForm;