import TodoForm from './components/TodoForm'
import TodoList from './components/TodoList'
import { useState } from 'react'
import './App.css';

function App() {

  // var for fetching the todos from local storage
  const fetchTodos = JSON.parse(localStorage.getItem("todo"));

  const [todos, setTodos] = useState(fetchTodos || []);

  // add todo function
  const addTodo = (todoAdd) => {
    const newTodos = [todoAdd, ...todos];

    setTodos(newTodos)
    localStorage.setItem("todo", JSON.stringify(newTodos))
  }

  // delete todo function
  const deleteTodo = (todoDelete) => {
    const newTodos = fetchTodos.filter((todo) => todo !== todoDelete);
    
    setTodos(newTodos)
    localStorage.setItem("todo", JSON.stringify(newTodos))
  }

  // toggle todo isDone function
  const toggleTodo = (todoIsDone) => {
    const newTodos = [...fetchTodos];

    newTodos.forEach((todo) => {
      if (todo === todoIsDone) {
        todo.isDone = !todoIsDone.isDone;
      }
    })

    setTodos(newTodos)
    localStorage.setItem("todo", JSON.stringify(newTodos))
  }

  // edit todo function 
  const editTodo = (todoEdit, newTodoTitle) => {
    const newTodos = [...fetchTodos];

    newTodos.forEach((todo) => {
      if (todo === todoEdit.todo) {
        todo.title = newTodoTitle;
      }
    })

    setTodos(newTodos)
    localStorage.setItem("todo", JSON.stringify(newTodos))
  }

  // return the main components and all the functions above passed as param
  return (
    <div className="App">
      <TodoForm 
        fetchTodos = {fetchTodos}
        addTodo = {addTodo}
      />
      <TodoList 
        fetchTodos = {fetchTodos} 
        deleteTodo = {deleteTodo}
        toggleTodo = {toggleTodo}
        editTodo = {editTodo}
      />
    </div>
  );
}

export default App;
